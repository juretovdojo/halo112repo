package com.spring.juretovdojo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import com.spring.juretovdojo.model.Korisnik;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

    @Query("SELECT u FROM Korisnik u where registriran = false")
    List<Korisnik> getAllNonregisteredUsers();

    @Query("SELECT u FROM Korisnik u where registriran = true and vrstaSpecijalizacije <> '0'")
    List<Korisnik> getAllRegisteredUsers();

    @Modifying
    Long deleteByEmail(String email);

    Optional<Korisnik> getByEmail(String email);


}
