package com.spring.juretovdojo.repository;

import com.spring.juretovdojo.model.Dispecer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DispecerRepository extends JpaRepository<Dispecer, Long>
{
    void deleteById(Long id);
}
