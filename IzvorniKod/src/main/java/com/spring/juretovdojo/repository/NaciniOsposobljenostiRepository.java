package com.spring.juretovdojo.repository;

import com.spring.juretovdojo.model.NaciniOsposobljenosti;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NaciniOsposobljenostiRepository extends JpaRepository<NaciniOsposobljenosti, Long> {

}
