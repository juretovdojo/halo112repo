package com.spring.juretovdojo.repository;

import com.spring.juretovdojo.model.Fotografija;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FotografijaRepository extends JpaRepository<Fotografija, Long>{
    
}
