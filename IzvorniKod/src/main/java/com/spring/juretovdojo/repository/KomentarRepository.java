package com.spring.juretovdojo.repository;

import java.util.List;

import com.spring.juretovdojo.model.Komentar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KomentarRepository extends JpaRepository<Komentar, Long> {

    List<Komentar> findAllByIdKorisnik(Long id);

    List<Komentar> findAllByIdAkcija(Long idAkcija);
}
