package com.spring.juretovdojo.repository;

import java.util.List;

import com.spring.juretovdojo.model.Zadatak;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZadatakRepository extends JpaRepository<Zadatak, Long> {

    List<Zadatak> findAllByIdSpasilacAndIdAkcija(Long idSpasilac, Long idAkcija);

    List<Zadatak> findAllByIdAkcija(Long idAkcija);
}
