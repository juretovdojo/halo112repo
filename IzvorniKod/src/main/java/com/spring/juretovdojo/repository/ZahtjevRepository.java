package com.spring.juretovdojo.repository;

import java.util.List;

import com.spring.juretovdojo.model.Zahtjev;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZahtjevRepository extends JpaRepository<Zahtjev, Long> {

    List<Zahtjev> findAllByidSpasilac(Long idspasilac);
}
