package com.spring.juretovdojo.repository;

import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StanicaRepository extends JpaRepository<Stanica, Long> {

    Boolean existsStanicaByVoditelj(Spasilac spasilac);
}
