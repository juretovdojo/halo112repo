package com.spring.juretovdojo.repository;


import com.spring.juretovdojo.model.Administrator;

import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {

}
