package com.spring.juretovdojo.repository;

import java.util.List;

import com.spring.juretovdojo.model.Spasilac;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SpasilacRepository extends JpaRepository<Spasilac, Long> {
    
    void deleteById(Long id);

    @Query("SELECT s FROM Spasilac s where IdStanica = NULL")
    List<Spasilac> getAllNoStationUsers();

    @Query("SELECT s FROM Spasilac s where IdStanica = ?1")
    List<Spasilac> getAllStationUsers(Long stanicaId);

    List<Spasilac> findAllByIdAkcija(Long idAction);
}
