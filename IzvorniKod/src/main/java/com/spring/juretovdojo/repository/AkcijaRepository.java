package com.spring.juretovdojo.repository;

import java.util.List;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AkcijaRepository extends JpaRepository<Akcija, Long> {

    List<Akcija> findAllByPokretac(Dispecer Dispecer);
}
