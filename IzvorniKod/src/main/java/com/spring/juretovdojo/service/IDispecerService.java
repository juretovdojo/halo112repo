package com.spring.juretovdojo.service;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;

public interface IDispecerService {

    public boolean isDispecer(long id);

    public void saveDispecer(Korisnik korisnik);

    public void saveDispecer(Dispecer dispecer);

    public void deleteDispecerById(Long id);
    
}
