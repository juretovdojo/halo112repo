package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.NaciniOsposobljenosti;
import com.spring.juretovdojo.repository.NaciniOsposobljenostiRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NaciniOsposobljenostiService implements INaciniOsposobljenostiService {

    @Autowired
    private NaciniOsposobljenostiRepository naciniOspRepo;

    @Override
    public List<NaciniOsposobljenosti> getAll() {
        return naciniOspRepo.findAll();
    }

    @Override
    public List<NaciniOsposobljenosti> getAllByListId(List<Long> idsOsposobljenosti) {
        return naciniOspRepo.findAllById(idsOsposobljenosti);
    }

    public List<NaciniOsposobljenosti> getAllByIds(List<Long> listIds) {
        return naciniOspRepo.findAllById(listIds);
    }
    
}
