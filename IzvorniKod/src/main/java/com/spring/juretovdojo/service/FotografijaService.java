package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.spring.juretovdojo.model.Fotografija;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FotografijaService implements IFotografijaService {

    public static byte[] convertPhoto(MultipartFile photo) throws IOException {
        String encodedPhoto = Base64.getEncoder().encodeToString(photo.getBytes());
        byte[] codedPhoto = encodedPhoto.getBytes();
        return codedPhoto;
    }

    public List<byte[]> convertListMultiPartFileToBytes(MultipartFile[] fotografije) throws IOException {
        List<byte[]> listByteFotografije = new ArrayList<>();
        for (MultipartFile f : fotografije) {
            listByteFotografije.add(convertPhoto(f));
        }
        return listByteFotografije;
    }

    public Set<Fotografija> ToSetFotografija(MultipartFile[] fotografije) throws IOException {

        List<byte[]> byteFotografije = convertListMultiPartFileToBytes(fotografije);
        Set<Fotografija> setFotografija = new HashSet<>();
        for (byte[] bs : byteFotografije) {
            Fotografija foto = new Fotografija();
            foto.setFotografija(bs);
            setFotografija.add(foto);
        }
        return setFotografija;
    }
}
