package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.List;

import com.spring.juretovdojo.model.Korisnik;

import org.springframework.web.multipart.MultipartFile;

public interface IKorisnikService {
    
    
    void saveKorisnik(Korisnik korisnik);
    
    void saveKorisnik(Korisnik korisnik, MultipartFile photo) throws IOException;

    void editKorisnik(Korisnik korisnik);

    Korisnik logginKorisnik(String email, String password);

    Korisnik getKorisnikById(long id);
    
    Korisnik getKorisnikByEmail(String email);

    void registerOrDeleteKorisnik(long id, boolean register);
    
    List<Korisnik> getAllNonregisteredUsers();
    
    List<Korisnik> getAllRegisteredUsers();

    boolean checkIfExists(Korisnik korisnik);
    
    void deleteByEmail(String email);

    void deleteKorisnik(Korisnik korisnik);

    void deleteKorisnikById(Long id);
}
