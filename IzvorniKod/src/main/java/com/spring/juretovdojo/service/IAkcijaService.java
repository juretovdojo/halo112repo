package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.List;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;

import org.springframework.web.multipart.MultipartFile;

public interface IAkcijaService {

    void createAction(String location, Dispecer dispecer, MultipartFile[] fotografije) throws IOException;

    List<Akcija> getDispecerActions(Dispecer dispecer);

    List<Akcija> getAllActions();
    
}
