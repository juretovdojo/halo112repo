package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;

public interface IStanicaService {

    Stanica createStation(String imeStanice, Long idVoditelj, String lokacija);

    Stanica getStanica(Long id);
    
    List<Stanica> getAllStations();

    Stanica editStanica(Stanica stanica, Long idVoditelj, Long idLokacija);

    Stanica editStanica(Stanica stanica);

    Stanica saveStanica(Stanica stanica);

    Stanica saveStanica(Stanica stanica, Long idVoditelj, Long idLokacija);

    boolean isStanicaValid(Stanica stanica);

    Boolean isVoditelj(Spasilac spasilac);
}
