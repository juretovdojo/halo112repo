package com.spring.juretovdojo.service;

import java.util.List;
import java.util.Optional;

import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;
import com.spring.juretovdojo.repository.StanicaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StanicaService implements IStanicaService {

    @Autowired
    private StanicaRepository stanicaRepository;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private LokacijaService lokacijaService;

    @Override
    public Stanica createStation(String imeStanice, Long idVoditelj, String lokacija) {
        Lokacija location = LokacijaService.createLokacija(lokacija, imeStanice);

        Spasilac voditelj = spasilacService.getSpasilac(idVoditelj);

        Stanica stanica = new Stanica();

        stanica.setImeStanice(imeStanice);

        stanica.setLokacija(location);

        stanica.setVoditelj(voditelj);

        spasilacService.saveStanicaForSpasilac(stanica.getVoditelj(), stanica);

        return saveStanica(stanica);
    }
    
    @Override
    public boolean isStanicaValid(Stanica stanica) {
        if(isVoditelj(stanica.getVoditelj())) {
            throw new RuntimeException("Taj spasilac je već voditelj");
        };
        return true;
    }

    @Override
    public Stanica getStanica(Long id) {
        Optional<Stanica> stanica = stanicaRepository.findById(id);
        if (stanica.isPresent()) return stanica.get();

        String error = String.format("Ne postoji stanica s id-em: %L", id);
        throw new RuntimeException(error);
    }

    @Override
    public List<Stanica> getAllStations() {
        return stanicaRepository.findAll();
    }

    @Override
    public Stanica editStanica(Stanica stanica) {
        if (this.isStanicaValid(stanica)) {
            return stanicaRepository.save(stanica);
        }
        else {
            throw new RuntimeException("ne valja stanica");
        }
    }

    @Override
    public Stanica editStanica(Stanica stanica, Long idVoditelj, Long idLokacija) {
        Spasilac voditelj = spasilacService.getSpasilac(idVoditelj);
        stanica.setVoditelj(voditelj);

        Lokacija lokacija = lokacijaService.getLokacija(idLokacija);
        stanica.setLokacija(lokacija);
        return this.saveStanica(stanica);
    }

    @Override
    public Stanica saveStanica(Stanica stanica) {

        if (isStanicaValid(stanica)) {
            return stanicaRepository.save(stanica);
        }
        else {
            throw new RuntimeException("ne valja stanica");
        }
    }

    @Override
    public Stanica saveStanica(Stanica stanica, Long idVoditelj, Long idLokacija) {
        Spasilac voditelj = spasilacService.getSpasilac(idVoditelj);
        stanica.setVoditelj(voditelj);

        Lokacija lokacija = lokacijaService.getLokacija(idLokacija);
        stanica.setLokacija(lokacija);

        return this.saveStanica(stanica);
    }

    public Boolean isVoditelj(Spasilac spasilac) {
        if (spasilac == null) return false;
        return stanicaRepository.existsStanicaByVoditelj(spasilac);
    }
}
