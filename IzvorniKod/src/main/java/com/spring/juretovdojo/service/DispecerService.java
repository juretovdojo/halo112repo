package com.spring.juretovdojo.service;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.repository.DispecerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DispecerService implements IDispecerService {

    @Autowired
    private DispecerRepository dispecerRepo;
    
    @Override
    public boolean isDispecer(long id) {
        return dispecerRepo.existsById(id);
    }

    @Override
    public void saveDispecer(Korisnik korisnik) {
        if (korisnik.getVrstaSpecijalizacije() != UserType.DISPATCHER) {
            String ispis = String.format("Korisnik nije dispecer: %s\n",korisnik);
            throw new RuntimeException(ispis);
        }
        Dispecer dispecer = new Dispecer(korisnik);
        this.saveDispecer(dispecer);
    }

    @Override
    public void saveDispecer(Dispecer dispecer) {
        this.dispecerRepo.save(dispecer);
    }

    @Override
    public void deleteDispecerById(Long id) {
        dispecerRepo.deleteById(id);

    }

}
