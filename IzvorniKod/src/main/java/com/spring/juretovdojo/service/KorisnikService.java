package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.repository.KorisnikRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class KorisnikService implements IKorisnikService
{
    @Autowired
    private KorisnikRepository korisnikRepo;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private DispecerService dispecerService;
    
    @Override
    public void saveKorisnik(Korisnik korisnik) {
         // provjera je li manualno admin stavljen
         if (korisnik.getVrstaSpecijalizacije() == UserType.ADMIN)
         {
             throw new RuntimeException("Nešto je pogrešno");
         }
         //provjera je li polje prazno
         else if (korisnik.signinWrong())
         {
             System.out.println("\u001B[31m"+"\nNEPOTPUN REGISTER\n"+"\u001B[0m");
             throw new RuntimeException("fale stvari\n");
         }
         //provjera postoji li korisnik s unique kredencijalima u bazi vec
         else if (this.checkIfExists(korisnik))
         {
            System.out.println("\u001B[31m"+"\nNEPOTPUN REGISTER\n"+"\u001B[0m");
            throw new RuntimeException("Postoji korisnik s istim korisničkim imenom ili emailom!");
         }
         String msg = korisnik.validateUser();
        if (msg != "OK")
        {
            System.out.println("\u001B[31m"+"\nPOGRESKA U REGISTRACIJI!\n"+"\u001B[0m");
        	throw new RuntimeException(msg);
        }
        this.korisnikRepo.save(korisnik);
        System.out.println("\u001B[31m"+"\nSPREMAMO KORISNIKA\n"+"\u001B[0m");
        System.out.println(korisnik);
    }
    
    @Override
    public void saveKorisnik(Korisnik korisnik, MultipartFile photo) throws IOException
    {
        String encodedPhoto = Base64.getEncoder().encodeToString(photo.getBytes());
        byte[] codedPhoto = encodedPhoto.getBytes();
        korisnik.setFotografija(codedPhoto);
        this.saveKorisnik(korisnik);
    }

    @Override
    public void editKorisnik(Korisnik korisnik) {
        korisnik.setRegistriran(true);
        Korisnik oldKorisnik = korisnikRepo.getById(korisnik.getIdKorisnik());
        if (oldKorisnik == null) {
            String error = String.format("Ne postoji korisnik s id-em: %s\n", korisnik.getIdKorisnik());
            throw new RuntimeException(error);
        }
        korisnik.setFotografija(oldKorisnik.getFotografija());
        String msg = korisnik.validateUser();
        if (msg != "OK") {
            throw new RuntimeException(msg);
        }
        HashSet<UserType> saviours = new HashSet<UserType>();
        saviours.add(UserType.DOCTOR);
        saviours.add(UserType.FIREMAN);
        saviours.add(UserType.POLICEMAN);
        if (korisnik.getVrstaSpecijalizacije() == UserType.DISPATCHER && saviours.contains(oldKorisnik.getVrstaSpecijalizacije())) {
            //bio je spasilac sad je dispecer
            System.out.printf("Bio je spasilac %s", oldKorisnik.getSpasilac());
            Dispecer dispecer = new Dispecer(korisnik);
            korisnik.setDispecer(dispecer);
            oldKorisnik.setSpasilac(null);
            spasilacService.deleteSpasilacById(korisnik.getIdKorisnik());
        }

        else if (oldKorisnik.getVrstaSpecijalizacije() == UserType.DISPATCHER && saviours.contains(korisnik.getVrstaSpecijalizacije())) {
           //bio je dispecer sad je spasilac
           System.out.printf("Bio je dispecer %s", oldKorisnik.getDispecer());
           Spasilac spasilac = new Spasilac(korisnik);
           korisnik.setSpasilac(spasilac);
           oldKorisnik.setDispecer(null);
           dispecerService.deleteDispecerById(korisnik.getIdKorisnik());
        }
        System.out.println("\u001B[31m"+"\nPOCETAK UPDATEA BAZE\n"+"\u001B[0m");
        korisnikRepo.save(korisnik);
    }

    @Override
    public Korisnik logginKorisnik(String email, String password) {
        Korisnik mybKor = this.getKorisnikByEmail(email);
        if (mybKor == null || !(password.equals(mybKor.getLozinka())) || !mybKor.isRegistriran())
        {
            System.out.printf("Neuspjeli login\n");
            throw new RuntimeException("Pogrešan login");
        }
        System.out.printf("Uspjeli login %s\n", mybKor);
        return mybKor;
    }

    @Override
    public Korisnik getKorisnikById(long id) {
        Optional<Korisnik> optional = korisnikRepo.findById(id);
        if (optional.isPresent())
        {
            return optional.get();
        }
        else
        {
            throw new RuntimeException("Korisnik nije pronaden s id: " + id);
        }
    }

    @Override
    public Korisnik getKorisnikByEmail(String email)
    {
        Optional<Korisnik> optional = korisnikRepo.getByEmail(email);
        if (optional.isPresent())
        {
            return optional.get();
        }
        else
        {
            throw new RuntimeException("Korisnik nije pronaden s emailom: " + email);
        }

    }

    @Override
    public List<Korisnik> getAllNonregisteredUsers()
    {
        return this.korisnikRepo.getAllNonregisteredUsers();
    }

    @Override
    public List<Korisnik> getAllRegisteredUsers() {
        return this.korisnikRepo.getAllRegisteredUsers();
    }

    @Override
    public boolean checkIfExists(Korisnik korisnik)
    {
        List<Korisnik> sviKorisnici = this.getAllRegisteredUsers();
        for (Korisnik k : sviKorisnici)
        {
            if (korisnik.getIdKorisnik() != k.getIdKorisnik() && 
            (korisnik.getKorisnickoIme().equals(k.getKorisnickoIme())
            || korisnik.getEmail().equals(k.getEmail())
            || korisnik.getBrojMobitela().equals(k.getBrojMobitela())))
            {
                System.out.println("\u001B[31m"+"\n!!!KORISNIK POSTOJI\n"+"\u001B[0m");
                return true;
            }
        }
        return false;
    }

    @Override
    public void deleteByEmail(String email) {
        this.korisnikRepo.deleteByEmail(email);
    }

    @Override
    public void registerOrDeleteKorisnik(long id, boolean register) {
        //System.out.println("\u001B[31m"+"\nPOCETAK UPDATEA BAZE\n"+"\u001B[0m");
        Korisnik korisnik = korisnikRepo.getById(id);
        if (register) {
            System.out.printf("\u001B[31m"+"\nUPDATE NA KORISNIKU\n%s\n"+"\u001B[0m", korisnik);
            korisnik.setRegistriran(true);
            
            if (korisnik.getVrstaSpecijalizacije() == UserType.DISPATCHER) {
                Dispecer dispecer = new Dispecer(korisnik.getIdKorisnik());
                korisnik.setDispecer(dispecer);
                // dispecerService.saveDispecer(korisnik);
            }
            List<UserType> tipoviSpasilaca = Arrays.asList(UserType.DOCTOR, UserType.FIREMAN, UserType.POLICEMAN);
            if (tipoviSpasilaca.contains(korisnik.getVrstaSpecijalizacije())) {
                Spasilac spasilac = new Spasilac(korisnik);
                korisnik.setSpasilac(spasilac);
            }
            this.saveKorisnik(korisnik);
        }
        else {
            this.deleteKorisnik(korisnik);
        }
    }

    @Override
    public void deleteKorisnik(Korisnik korisnik) {
        //izbrisi iz baze
        System.out.printf("\u001B[31m"+"\nBRIŠEM KORISNIKA\n%s\n"+"\u001B[0m", korisnik);
        korisnikRepo.delete(korisnik);
    }

    @Override
    public void deleteKorisnikById(Long id) {
        System.out.printf("\u001B[31m"+"\nBRIŠEM KORISNIKA\n%s\n"+"\u001B[0m", id);
        korisnikRepo.deleteById(id);
    }
}
