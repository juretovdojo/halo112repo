package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.Dispecer;

public interface IZahtjevService {

    void createZahtjevi(Dispecer dispecer, Long idAkcija, List<Long> idTransports, Integer razinaHitnosti, String info);
    
}
