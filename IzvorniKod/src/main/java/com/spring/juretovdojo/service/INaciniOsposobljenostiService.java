package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.NaciniOsposobljenosti;

public interface INaciniOsposobljenostiService {
    List<NaciniOsposobljenosti> getAll();

    List<NaciniOsposobljenosti> getAllByListId(List<Long> idsOsposobljenosti);
}
