package com.spring.juretovdojo.service;

import java.util.Optional;

import com.spring.juretovdojo.model.Administrator;
import com.spring.juretovdojo.repository.AdministratorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministratorService implements IAdministratorService
{
    @Autowired
    private AdministratorRepository administratorRepo;

    @Override
    public boolean isAdmin(long id) {
        return administratorRepo.existsById(id);
    }

    @Override
    public Administrator getAdmin(long id) {
        Optional<Administrator> optional = administratorRepo.findById(id);
        if (optional.isPresent())
        {
            return optional.get();
        }
        return null;
    }
}
