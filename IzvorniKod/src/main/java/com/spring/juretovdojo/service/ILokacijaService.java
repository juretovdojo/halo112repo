package com.spring.juretovdojo.service;

import com.spring.juretovdojo.model.Lokacija;

public interface ILokacijaService {

    void saveLokacija(Lokacija lokacija);

    Lokacija getLokacija(Long id);

    Iterable<Lokacija> getAllLokacija();

    
    
}
