package com.spring.juretovdojo.service;

import java.util.ArrayList;
import java.util.List;

import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Zadatak;
import com.spring.juretovdojo.repository.ZadatakRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZadatakService implements IZadatakService {

    @Autowired
    private ZadatakRepository zadatakRepository;

    public void saveZadatak(Zadatak zadatak) {
        zadatakRepository.save(zadatak);
    }

    public void makeZadatak(Long idSaviour, Long idDispecer, Long idAkcija, String komentar, String location) {
        Lokacija lokacija = LokacijaService.createLokacija(location, idSaviour.toString());
        Zadatak zadatak = new Zadatak();
        zadatak.setIdDispecer(idDispecer);
        zadatak.setIdSpasilac(idSaviour);
        zadatak.setLokacija(lokacija);
        zadatak.setOpis(komentar);
        zadatak.setIdAkcija(idAkcija);
        System.out.println(zadatak);
        zadatakRepository.save(zadatak);
    }

    public List<Zadatak> getAll() {
        return zadatakRepository.findAll();
    }

    public List<Zadatak> getAllTasksForSpasilac(Spasilac sp) {
        if (sp == null) return new ArrayList<>();
        return zadatakRepository.findAllByIdSpasilacAndIdAkcija(sp.getIdSpasilac(), sp.getIdAkcije());
    }

    public List<Zadatak> getAllTasksOnAction(Long idAkcija) {
        return zadatakRepository.findAllByIdAkcija(idAkcija);
    }
}
