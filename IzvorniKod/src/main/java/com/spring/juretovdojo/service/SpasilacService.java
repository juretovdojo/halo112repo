package com.spring.juretovdojo.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.NaciniOsposobljenosti;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.repository.SpasilacRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpasilacService implements ISpasilacService {
    
    @Autowired
    private SpasilacRepository spasilacRepo;

    @Autowired
    private NaciniOsposobljenostiService naciniOspService;

    @Autowired
    private LokacijaService lokacijaService;

    @Autowired
    private AkcijaService akcijaService;

    @Autowired
    private StanicaService stanicaService;
    
    @Override
    public boolean isSpasilac(long id) {
        return spasilacRepo.existsById(id);
    }

    public Spasilac getSpasilac(Spasilac spasilac) {
        if (spasilac == null) return null;
        return getSpasilac(spasilac.getIdSpasilac());
    }

    @Override
    public Spasilac getSpasilac(Long id) {
        Optional<Spasilac> spasilac = spasilacRepo.findById(id);
        if (!spasilac.isPresent()) {
            String error = String.format("Ne postoji spasilac s id-em: %L", id);
            throw new RuntimeException(error);
        }
        return spasilac.get();
    }
    
    @Override
    public List<Spasilac> getAllSpasilac() {
        List<Spasilac> spasioci = spasilacRepo.findAll();
        return spasioci;
    }
    
    public List<Spasilac> getAllNoStationSpasilac() {
        List<Spasilac> spasioci = spasilacRepo.getAllNoStationUsers();
        return spasioci;
    }
    
    public List<Spasilac> getAllStationSpasilac(long stanicaId) {
        List<Spasilac> spasioci = spasilacRepo.getAllStationUsers(stanicaId);
        return spasioci;
    }

    /*public List<Spasilac> getAllActionSpasilac(long actionId) {
        List<Spasilac> spasioci = spasilacRepo.getAllActionUsers(actionId);
        return spasioci;
    }*/

    @Override
    public void saveSpasilac(Korisnik korisnik) {
        if (korisnik.getVrstaSpecijalizacije() == UserType.ADMIN || 
        korisnik.getVrstaSpecijalizacije() == UserType.DISPATCHER) {
            String ispis =String.format("Korisnik nije spasilac: %s\n",korisnik);
            throw new RuntimeException(ispis);
        }
        Spasilac spasilac = new Spasilac(korisnik);
        this.spasilacRepo.save(spasilac);
    }

    public void saveStanicaForSpasilac(Long idSpasilac, Stanica station) {
        Spasilac spasilac = getSpasilac(idSpasilac);
        spasilac.setIdStanica(station.getIdStanica());
        spasilacRepo.save(spasilac);
    }

    public void saveStanicaForSpasilac(Spasilac spasilac, Stanica station) {
        spasilac.setIdStanica(station.getIdStanica());
        spasilacRepo.save(spasilac);
    }
    
    @Override
    public void addSpasilacToStanica(Long Idspasilac, Long idStanica) {
        Spasilac spasilac = getSpasilac(Idspasilac);
        spasilac.setIdStanica(idStanica);
        spasilacRepo.save(spasilac);
    }

    @Override
    public void addSpasilaciToStanica(List<Long> spasilaci, Long idStanica) {
        spasilaci.forEach(idSpasilac -> this.addSpasilacToStanica(idSpasilac, idStanica));
    }

    @Override
    public void setTransportsForSpasilac(Long idSpasilac, List<Long> idsTransports) {
        Spasilac spasilac = getSpasilac(idSpasilac);
        List<NaciniOsposobljenosti> osposobljenosti = naciniOspService.getAllByListId(idsTransports);
        spasilac.setOsposobljenosti(osposobljenosti);
        spasilacRepo.save(spasilac);
    }

    @Override
    public void saveSpasilac(Spasilac spasilac) {
        spasilacRepo.save(spasilac);
    }

    @Override
    public void deleteSpasilacById(Long id) {
        spasilacRepo.deleteById(id);
    }

    public Spasilac saveSpasilacLocation(Spasilac spasilac, String sirina, String duzina) {
        Lokacija lokacija = LokacijaService.createLokacija(sirina, duzina, spasilac.getKorisnik().getKorisnickoIme());
        Spasilac sp = getSpasilac(spasilac.getIdSpasilac());
        System.out.println("\n\nspasilac koji zeza"+sp);
        Long lk = null;
        if (sp.getLokacija() != null) {
            lk = sp.getLokacija().getIdLokacija();
            System.out.println("lokacija" + lk);
        }
        if (lk != null) {
            lokacijaService.deleteLokacija(lk);
        }
        sp.setLokacija(lokacija);
        saveSpasilac(sp);
        return sp;
    }

    public List<Spasilac> getAllSpasilacForAkcija(Long idAction) {
        return spasilacRepo.findAllByIdAkcija(idAction);
    }

    public Set<Spasilac> getAllOnSpasilacAction(Spasilac spasilac) {
        if (spasilac == null) return new HashSet<>();
        if (spasilac.getIdAkcije() == null) return new HashSet<>();;
        Akcija akcija = akcijaService.getAkcija(spasilac.getIdAkcije());
        Set<Spasilac> spasilaci = akcija.getSpasilaci();
        spasilaci.remove(spasilac);
        return akcija.getSpasilaci();
    }

    public void removeActionFromSpasilac(Long idAkcija) {
        List<Spasilac> spasilaci = spasilacRepo.findAllByIdAkcija(idAkcija);
        spasilaci.stream().forEach(s -> s.setIdAkcije(null));
        spasilacRepo.saveAll(spasilaci);
    }

    public void removeSaviourFromStation(Long id) {
        Spasilac spasilac = getSpasilac(id);
        if (stanicaService.isVoditelj(spasilac)) {
            throw new RuntimeException("Ne možete voditelja maknut iz stanice");
        }
        spasilac.setIdStanica(null);
        saveSpasilac(spasilac);
    }

    public void removeFromSaviourAction(Long idSpasilac) {
        Spasilac spasilac = getSpasilac(idSpasilac);
        spasilac.setIdAkcije(null);
        saveSpasilac(spasilac);
    }
}
