package com.spring.juretovdojo.service;

import com.spring.juretovdojo.model.Administrator;

public interface IAdministratorService {
    boolean isAdmin(long id);
    Administrator getAdmin(long id);
}
