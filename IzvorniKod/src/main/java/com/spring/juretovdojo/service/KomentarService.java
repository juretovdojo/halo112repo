package com.spring.juretovdojo.service;

import java.util.ArrayList;
import java.util.List;

import com.spring.juretovdojo.model.Komentar;
import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.repository.KomentarRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KomentarService implements IKomentarService{

    @Autowired
    private KomentarRepository komentarRepository;

    public void makeKomentar(String lokacija, Long idKorisnik, Long idAction, String tekst) {

        Lokacija location = LokacijaService.createLokacija(lokacija, idKorisnik.toString()+"k");
        Komentar komentar = new Komentar();
        komentar.setIdAkcija(idAction);
        komentar.setIdKorisnik(idKorisnik);
        komentar.setLokacija(location);
        komentar.setTekst(tekst);
        komentarRepository.save(komentar);
    }

    public List<Komentar> getAll() {
        return komentarRepository.findAll();
    }

    public List<Komentar> findKomentariOnSameAction(Spasilac sp) {
        if (sp == null) return new ArrayList<>();
        return komentarRepository.findAllByIdKorisnik(sp.getIdSpasilac());
    }

    public List<Komentar> getAllCommentOnAction(Long idAkcija) {
        return komentarRepository.findAllByIdAkcija(idAkcija);
    }
}
