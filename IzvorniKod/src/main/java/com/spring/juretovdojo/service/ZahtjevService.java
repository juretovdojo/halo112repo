package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.NaciniOsposobljenosti;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Zahtjev;
import com.spring.juretovdojo.repository.ZahtjevRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZahtjevService implements IZahtjevService {

    @Autowired
    private ZahtjevRepository zahtjevRepository;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private NaciniOsposobljenostiService naciniOsposobljenostiService;

    @Autowired
    private AkcijaService akcijaService;

    public Zahtjev getZahtjev(Long id) {
        return zahtjevRepository.getById(id);
    }

    @Override
    public void createZahtjevi(Dispecer dispecer, Long idAction, List<Long> idTransports, Integer razinaHitnosti, String info) {
        List<NaciniOsposobljenosti> osposobljenosti = naciniOsposobljenostiService.getAllByIds(idTransports);
        List<Spasilac> spasilaci = spasilacService.getAllSpasilac();
        for (Spasilac spasilac : spasilaci) {
            if(spasilac.getDostupnost()) {
                if (spasilac.getOsposobljenosti().stream().anyMatch(osposobljenosti::contains)) {
                    createZahtjev(spasilac, dispecer, idAction, razinaHitnosti, info);
                }
            }
        }
    }

    private void createZahtjev(Spasilac spasilac, Dispecer dispecer, Long idAkcija, Integer razinaHitnosti, String info) {
        Zahtjev zahtjev = new Zahtjev();
        zahtjev.setIdDispecer(dispecer.getIdDispecer());
        zahtjev.setIdAkcija(idAkcija);
        zahtjev.setRazinaHitnosti(razinaHitnosti);
        zahtjev.setInformacija(info);
        zahtjev.setIdSpasilac(spasilac.getIdSpasilac());
        zahtjevRepository.save(zahtjev);
    }

    public List<Zahtjev> getZahtjeviForSpasilac(Spasilac spasilac) {
        return zahtjevRepository.findAllByidSpasilac(spasilac.getIdSpasilac());
    }

    public Akcija prihvatiZahtjev(Long idZahtjev, Spasilac spasilac) {
        Zahtjev zahtjev = getZahtjev(idZahtjev);
        spasilac.setIdAkcije(zahtjev.getIdAkcija());
        spasilac.setDostupnost(false);
        spasilacService.saveSpasilac(spasilac);
        deleteZahtjev(zahtjev);
        return akcijaService.getAkcija(zahtjev.getIdAkcija());
    }

    public void odbiZahtjev(Long idZahtjev) {
        zahtjevRepository.deleteById(idZahtjev);
    }

    public void deleteZahtjev(Zahtjev zahtjev) {
        zahtjevRepository.delete(zahtjev);
    }
}
