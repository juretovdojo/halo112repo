package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Fotografija;
import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.repository.AkcijaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class AkcijaService implements IAkcijaService {

    @Autowired
    private AkcijaRepository akcijaRepository;

    @Autowired
    private FotografijaService fotografijaService;

    @Autowired
    private SpasilacService spasilacService;

    public Akcija getAkcija(Long id) {
        return akcijaRepository.getById(id);
    }

    @Override
    public void createAction(String location, Dispecer dispecer, MultipartFile[] fotografije) throws IOException {
        Akcija akcija = new Akcija();

        Lokacija lokacija = LokacijaService.createLokacija(location, "akcija");

        akcija.setLokacija(lokacija);
        akcija.setPokretac(dispecer);

        Set<Fotografija> setFotografija =  fotografijaService.ToSetFotografija(fotografije);

        akcija.setFotografije(setFotografija);

        akcijaRepository.save(akcija);
    }

    @Override
    public List<Akcija> getAllActions() {
        return akcijaRepository.findAll();
    }

    public List<Akcija> getDispecerActions(Dispecer dispecer) {
        List<Akcija> akcije = akcijaRepository.findAllByPokretac(dispecer);
        return akcije;
    }

    public Akcija getSpasilacAction(Spasilac spasilac) {
        if (spasilac.getIdAkcije() == null) return null;
        Optional<Akcija> akcija = akcijaRepository.findById(spasilac.getIdAkcije());
        if (akcija.isPresent()) {
            return akcija.get();
        }
        return null;
    }

    public void closeAction(Long idAkcija) {
        Akcija akcija = getAkcija(idAkcija);
        akcija.setStatus(false);
        spasilacService.removeActionFromSpasilac(idAkcija);
        akcijaRepository.save(akcija);
    }

    

    
}
