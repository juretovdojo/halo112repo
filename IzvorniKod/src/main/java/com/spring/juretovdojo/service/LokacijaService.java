package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.repository.LokacijaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LokacijaService implements ILokacijaService {

    @Autowired
    private LokacijaRepository lokacijaRepository;

    @Override
    public void saveLokacija(Lokacija lokacija) {
        lokacijaRepository.save(lokacija);
    }

    public static Lokacija createLokacija(String lokacija, String naziv) {
        String[] loc = lokacija.split("\\$");
        System.out.println("location=" + lokacija + " parsed:" + loc[0] + "\n");
        Lokacija location = new Lokacija(Float.parseFloat(loc[0]), Float.parseFloat(loc[1]), naziv);
        return location;
    }

    @Override
    public Lokacija getLokacija(Long id) {
        Lokacija lokacija = lokacijaRepository.getById(id);
        if (lokacija == null) {
            String ispis = String.format("Ne postoji lokacija s id-em %L", id);
            throw new RuntimeException(ispis);
        }
        return lokacija;
    }

    @Override
    public List<Lokacija> getAllLokacija() {
        List<Lokacija> lokacije = lokacijaRepository.findAll();
        return lokacije;
    }

    public static Lokacija createLokacija(String sirina, String duzina, String korisnickoIme) {
        Lokacija lokacija = new Lokacija();
        lokacija.setSirina(Float.parseFloat(sirina));
        lokacija.setDuzina(Float.parseFloat(duzina));
        lokacija.setLokacija(korisnickoIme);
        return lokacija;
    }

    public void deleteLokacija(Long id) {
        lokacijaRepository.deleteById(id);
    }
}
