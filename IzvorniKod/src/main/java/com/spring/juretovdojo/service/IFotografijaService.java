package com.spring.juretovdojo.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface IFotografijaService {

    List<byte[]> convertListMultiPartFileToBytes(MultipartFile[] fotografije) throws IOException;
    
}
