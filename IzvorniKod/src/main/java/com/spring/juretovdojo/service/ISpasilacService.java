package com.spring.juretovdojo.service;

import java.util.List;

import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;

public interface ISpasilacService {

    boolean isSpasilac(long id);
    
    void saveSpasilac(Korisnik korisnik);

    void saveSpasilac(Spasilac spasilac);
    
    void saveStanicaForSpasilac(Long idVoditelj, Stanica station);
    
    void saveStanicaForSpasilac(Spasilac spasilac, Stanica station);

    void addSpasilacToStanica(Long idSpasilac, Long idStanica);
    
    void addSpasilaciToStanica(List<Long> spasilaci, Long idStanica);
    
    void setTransportsForSpasilac(Long idSpasilac, List<Long> idTransports);

    Spasilac getSpasilac(Long id);
    
    public List<Spasilac> getAllSpasilac();

    public List<Spasilac> getAllNoStationSpasilac();

    public List<Spasilac> getAllStationSpasilac(long stanicaId);

    public void deleteSpasilacById(Long id);
}
