package com.spring.juretovdojo.controller;

import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Korisnik;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class LogoutController
{
    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    // @ModelAttribute("role")
    // public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
    //     if (loggedUser == null) return null;
    //     return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    // }

    // @ModelAttribute("isVoditelj")
    // public boolean isVoditelj() {
    //     return true;
    // }


    @GetMapping("/odjaviSe")
    public String logout(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser)
    {
        session.invalidate();
        System.out.println("ODJAVA");
        return "redirect:/home";
    }
}
