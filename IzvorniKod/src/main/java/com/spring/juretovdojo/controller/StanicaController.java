package com.spring.juretovdojo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Lokacija;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.AdministratorService;
import com.spring.juretovdojo.service.LokacijaService;
import com.spring.juretovdojo.service.SpasilacService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
public class StanicaController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired 
    private SpasilacService spasilacService;

    @Autowired
    private LokacijaService lokacijaService;

    @Autowired
    private StanicaService stanicaService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }
    
    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("loggedInType")
    public String loggedInType(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser.getSpasilac() != null) return "Spasilac";

        if (loggedUser.getDispecer() != null) return "Dispecer";

        if (loggedUser.getAdministrator() != null) return "Admin";
        return "greska";
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        Boolean isVoditelj = spasilac == null ? false : stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", isVoditelj);
        return isVoditelj;
    }

    @ModelAttribute("isDispecer")
    public Boolean isDispecer(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer != null;
    }

    @ModelAttribute("isSpasilac")
    public Boolean isSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac != null;
    }

    @ModelAttribute("isAdminLoggedIn")
    public Boolean isAdminLoggedIn(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
        boolean isAdmin = administratorService.isAdmin(loggedUser.getIdKorisnik());
        if (isAdmin) {
            session.setAttribute("isAdminLoggedIn", Boolean.TRUE);
            return Boolean.TRUE;
        }
        else {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
    }

    @ModelAttribute("allStations")
    public List<Map<String,String>> allStations() {
        List<Stanica> stanice = stanicaService.getAllStations();
        List<Map<String, String>> stations = new ArrayList<>();
        
        stanice.
        stream().forEach(s -> {
            Map<String, String> mapirano = new HashMap<>();
            mapirano.put("idStanice", s.getIdStanica().toString());
            mapirano.put("imeStanice", s.getImeStanice());
            mapirano.put("sirina", Float.toString(s.getLokacija().getSirina()));
            mapirano.put("duzina", Float.toString(s.getLokacija().getDuzina()));
            stations.add(mapirano);
        }
        );
        return stations;
    }
    
    @ModelAttribute("allSaviours")
    public Iterable<Spasilac> allSaviours() {
        return spasilacService.getAllSpasilac();
    }

    @ModelAttribute("allLocations")
    public Iterable<Lokacija> allLocations() {
        return lokacijaService.getAllLokacija();
    }

    @ModelAttribute("nazivstanice")
    public String korisnikStanica(@SessionAttribute(required = false) Spasilac spasilac) {
        String naziv = spasilac == null ? "" : stanicaService.getStanica(spasilac.getIdStanica()).getImeStanice();
        return naziv;
    }
    
    @GetMapping("/admin/createstation")
    public String createStation(Model model, @SessionAttribute(required = false) Boolean isAdminLoggedIn) {
        if (!isAdminLoggedIn) {
            return "redirect:/invalidCredentials";
        }
        model.addAttribute("station", new Stanica());
        return "createstation";
    }

    @PostMapping("/createStation")
    public String createStation(@RequestParam(name = "idVoditelj") Long idVoditelj,
    @RequestParam(name = "location") String lokacija, @RequestParam(name = "imeStanice") String imeStanice, RedirectAttributes redAttrs) {
        try {
            Stanica station = stanicaService.createStation(imeStanice, idVoditelj, lokacija);
            spasilacService.saveStanicaForSpasilac(station.getVoditelj(), station);
            return "redirect:/";
        }
        catch (RuntimeException err) {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/admin/createstation";
        }
    }

    @GetMapping("admin/stationlist")
    public String allStations(Model model, @SessionAttribute boolean isAdminLoggedIn) {
        if (!isAdminLoggedIn) {
            return "redirect:/invalidCredentials";
        }
        List<Stanica> allStations = stanicaService.getAllStations();
        model.addAttribute("allStations", allStations);
        return "stationlist";

    }

    @GetMapping("/admin/editstation/{id}")
    public String editStation(@PathVariable(value = "id") long id, Model model, @SessionAttribute boolean isAdminLoggedIn) {
        if (!isAdminLoggedIn) {
            return "redirect:/invalidCredentials";
        }
        Stanica stanica = stanicaService.getStanica(id);
        List<Spasilac> svi = (List<Spasilac>) model.getAttribute("allSaviours");
        List<Spasilac> sviBezVoditelja = svi.stream().filter(s -> 
        {
            return !stanicaService.isVoditelj(s) && (s.getIdStanica() != stanica.getIdStanica());
        }
        ).collect(Collectors.toList());
        model.addAttribute("station", stanica);
        model.addAttribute("allSaviours", sviBezVoditelja);
        return "editstation";
    }

    @PostMapping("admin/editstation")
    public String editStation(@ModelAttribute Stanica station, @RequestParam(name = "idVoditelj") Long idVoditelj,
    @RequestParam(name = "idLokacija") Long idLokacija, RedirectAttributes redAttrs) {
        try {
            Stanica stanica = stanicaService.editStanica(station, idVoditelj, idLokacija);
            spasilacService.saveStanicaForSpasilac(station.getVoditelj(), stanica);
            return "redirect:/";
        }
        catch (RuntimeException err) {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/admin/createstation";
        }
    }

    @GetMapping("/{nazivstanice}/stationsaviourlist")
    public String stationSaviorList(Model model, @SessionAttribute Spasilac spasilac,
    @SessionAttribute(required = false) boolean isVoditelj) {
        if (!isVoditelj) {
            return "redirect:/InvalidCredentials";
        } 
        System.out.println("spasilac" + spasilac);
        Long stanicaId = spasilac.getIdStanica();
        List<Spasilac> stationsaviours = spasilacService.getAllStationSpasilac(stanicaId);
        model.addAttribute("stationsaviours", stationsaviours);
        return "stationsaviourlist";
    }

    @GetMapping("/{nazivstanice}/addsaviour")
    public String addSaviour(Model model, @SessionAttribute(required = false) Boolean isVoditelj) {
        if (!isVoditelj){
            return "redirect:/InvalidCredentials";
        } 
        List<Spasilac> saviours = spasilacService.getAllNoStationSpasilac();
        model.addAttribute("saviours", saviours);
        return "addsaviour";
    }

    @PostMapping("/addSaviour")
    public String addSaviours(@RequestParam(name = "idSaviours") Long[] spasilaci, 
    @SessionAttribute(required = false) Spasilac spasilac, RedirectAttributes redAttrs) {
        Long idStanica = spasilac.getIdStanica();
        spasilacService.addSpasilaciToStanica(List.of(spasilaci), idStanica);
        redAttrs.addFlashAttribute("success", "Spasioci uspješno dodani");
        String nazivStanice = stanicaService.getStanica(spasilac.getIdStanica()).getImeStanice();
        return "redirect:/" + nazivStanice + "/stationsaviourlist";
    }
}
