package com.spring.juretovdojo.controller;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.AdministratorService;
import com.spring.juretovdojo.service.KorisnikService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
public class AdministratorController {
    
    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private StanicaService stanicaService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }

    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("isAdminLoggedIn")
    public Boolean isAdminLoggedIn(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
        boolean isAdmin = administratorService.isAdmin(loggedUser.getIdKorisnik());
        if (isAdmin) {
            session.setAttribute("isAdminLoggedIn", Boolean.TRUE);
            return Boolean.TRUE;
        }
        else {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
    }

    @GetMapping("/admin")
    public String home(Model model, @SessionAttribute(required = false) Boolean isAdminLoggedIn)
    {
        //provjeri je li admin ulogiran
        if (!isAdminLoggedIn)
        {
            return "redirect:/invalidCredentials";
        }
        List<Korisnik> nonRegisteredUsers = korisnikService.getAllNonregisteredUsers();
        nonRegisteredUsers.forEach((user) -> System.out.printf("nonregistered: %s\n", user));
        model.addAttribute("users", nonRegisteredUsers);
        return "admin";
    }

    @PostMapping("/register")
    public String register(@RequestParam(name = "id") long id, @RequestParam(name = "registered") Boolean register, @SessionAttribute boolean isAdminLoggedIn, RedirectAttributes redAttrs)
    {
        //provjeri je li admin ulogiran
        if (!isAdminLoggedIn)
        {
            return "redirect:/invalidCredentials";
        }
        try
        {
            korisnikService.registerOrDeleteKorisnik(id, register);
        }
        catch (RuntimeException err)
        {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/admin/edituser/" + id;
        }
        redAttrs.addFlashAttribute("success", "Korisnik uspješno prihvaćen");
        return "redirect:/admin";
    }

    @PostMapping("deleteUser")
    public String delete(Model model, @RequestParam(name = "id") Long id, RedirectAttributes redAttrs) {
        // TODO testirat
        korisnikService.deleteKorisnikById(id);
        List<Korisnik> registeredUsers = korisnikService.getAllRegisteredUsers();
        model.addAttribute("users", registeredUsers);
        redAttrs.addFlashAttribute("success", "Korisnik uspješno izbrisan");
        return "redirect:/admin/userlist/";
    }
}

