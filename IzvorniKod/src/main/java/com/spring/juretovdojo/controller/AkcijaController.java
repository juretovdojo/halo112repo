package com.spring.juretovdojo.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.AkcijaService;
import com.spring.juretovdojo.service.SpasilacService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AkcijaController {

    @Autowired
    private StanicaService stanicaService;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private AkcijaService akcijaService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }

    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        Boolean exists = spasilac == null ? false : stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("Status")
    public Boolean statusSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac == null ? false : spasilac.getDostupnost();
    }

    @ModelAttribute("isSpasilac")
    public boolean isSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac != null;
    }

    @ModelAttribute("isDispecer")
    public boolean isSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer != null;
    }

    @ModelAttribute("nazivstanice")
    public String korisnikStanica(@SessionAttribute(required = false) Spasilac spasilac) {
        Long idStanica = spasilac.getIdStanica();
        if (idStanica == null) return "";
        String naziv = spasilac == null ? "" : stanicaService.getStanica(idStanica).getImeStanice();
        return naziv;
    }
    
    // @GetMapping("dispecer/createaction")
    // public String createAction(Model model, @SessionAttribute(required = false) Dispecer dispecer) {
    //     if (dispecer == null) {
    //         return "redirect:/invalidCredentials";
    //     }
    //     model.addAttribute("action", new Akcija());

    //     List<NaciniOsposobljenosti> naciniOsp = naciniOsposobljenostiService.getAll();

    //     model.addAttribute("naciniOsposobljenosti", naciniOsp);
    //     return "createaction";
    // }

    @PostMapping("createAction")
    public String createAction(@RequestParam(name = "fotografije") MultipartFile[] fotografije,
    @RequestParam(name = "location") String location,
    @SessionAttribute(name = "dispecer") Dispecer dispecer, RedirectAttributes redAttrs) {
        try {
            akcijaService.createAction(location, dispecer, fotografije);
            redAttrs.addFlashAttribute("success", "Akcija uspješno kreirana");
        } catch (IOException err) {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/dispecer";
        }
        return "redirect:/dispecer";
    }

    @PostMapping("/removesaviour")
    public String removeSaviourFromAction(@RequestParam(name = "id") Long idSaviour, RedirectAttributes redAttrs) {
        spasilacService.removeFromSaviourAction(idSaviour);
        redAttrs.addFlashAttribute("success", "uspješno uklonjen s akcije");
        return "redirect:/dispecer";
    }

    @PostMapping("/closeAction")
    public String closeAction(@RequestParam(name = "actionId") Long idAkcija) {
        akcijaService.closeAction(idAkcija);
        return "redirect:/dispecer";
    }
}
