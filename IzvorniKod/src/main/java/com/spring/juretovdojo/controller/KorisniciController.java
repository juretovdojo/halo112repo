package com.spring.juretovdojo.controller;

import com.spring.juretovdojo.model.Administrator;
import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.AdministratorService;
import com.spring.juretovdojo.service.KomentarService;
import com.spring.juretovdojo.service.KorisnikService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

import javax.servlet.http.HttpSession;


@Controller
public class KorisniciController {
    
    @Autowired
    private AdministratorService administratorService;
    
    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private StanicaService stanicaService;

    @Autowired
    private KomentarService komentarService;

    
    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }
    
    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("nazivstanice")
    public String korisnikStanica(@SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return "";
        Long idStanica = spasilac.getIdStanica();
        if (idStanica == null) return "";
        String naziv = spasilac == null ? "" : stanicaService.getStanica(idStanica).getImeStanice();
        return naziv;
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("isAdminLoggedIn")
    public Boolean isAdminLoggedIn(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
        boolean isAdmin = administratorService.isAdmin(loggedUser.getIdKorisnik());
        if (isAdmin) {
            session.setAttribute("isAdminLoggedIn", Boolean.TRUE);
            return Boolean.TRUE;
        }
        else {
            session.setAttribute("isAdminLoggedIn", Boolean.FALSE);
            return Boolean.FALSE;
        }
    }


    @GetMapping("/admin/userlist")
    public String home(Model model, @SessionAttribute boolean isAdminLoggedIn)
    {
        //provjeri je li admin ulogiran
        if (!isAdminLoggedIn)
        {
            return "redirect:/invalidCredentials";
        }
        List<Korisnik> registeredUsers = korisnikService.getAllRegisteredUsers();
        model.addAttribute("users", registeredUsers);
        return "userlist";
    }

    @GetMapping("/admin/edituser/{id}")
    public String lista(@PathVariable(value = "id") long id, Model model, @SessionAttribute boolean isAdminLoggedIn)
    {
        if (!isAdminLoggedIn)
        {
            return "redirect:/invalidCredentials";
        }
        Korisnik User = korisnikService.getKorisnikById(id);
        model.addAttribute("user", User);
        return "edituser";
    }

    @PostMapping("/admin/edituser")
    public String editUser(@ModelAttribute(value = "user") Korisnik korisnik, RedirectAttributes redAttrs)
    {
        System.out.println(korisnik);
        try {
            korisnikService.editKorisnik(korisnik);
        } catch (RuntimeException err) {
            System.out.printf(err.getMessage());
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/admin/edituser/" + korisnik.getIdKorisnik();
        }
        return "redirect:/admin/userlist";
    }

    @PostMapping("/comment")
    public String makeComment(@RequestParam(name = "actionId") Long idAction,
    @RequestParam(name = "text") String tekst, @RequestParam(name = "location") String lokacija, @SessionAttribute(name = "loggedUser") Korisnik korisnik, RedirectAttributes redAttrs) {
        komentarService.makeKomentar(lokacija, korisnik.getIdKorisnik(), idAction, tekst);
        if (korisnik.getVrstaSpecijalizacije() == UserType.DISPATCHER) return "redirect:/dispecer";
        return "redirect:/spasilac";
    }

    @PostMapping("/deleteuser")
    public String deleteUser(@RequestParam(name = "id") Long idUser,
    @SessionAttribute(name = "admin", required = false) Administrator administrator, RedirectAttributes redAttrs) {
        Korisnik k = korisnikService.getKorisnikById(idUser);
        if (stanicaService.isVoditelj(k.getSpasilac())) {
            redAttrs.addFlashAttribute("error", "Taj korisnik je voditelj. Maknite mu status voditelja prvo");
            return "redirect:/admin/userlist";
        }
        try {
            korisnikService.deleteKorisnikById(idUser);
            redAttrs.addFlashAttribute("success", "uspješno izbrisan korisnik");
        } catch (Exception err) {
            redAttrs.addFlashAttribute("error", "neuspješno izbrisan korisnik");
        }
        return "redirect:/admin/userlist";
    }
}
