package com.spring.juretovdojo.controller;

import javax.servlet.http.HttpServletRequest;

import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.service.KorisnikService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

    @Autowired
    private KorisnikService korisnikService;
    
    // @ModelAttribute("loggedUser")
    // public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
    //     if (loggedUser == null) return null;
    //     return loggedUser;
    // }
    
    // @ModelAttribute("role")
    // public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
    //     if (loggedUser == null) return null;
    //     return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    // }

    // @ModelAttribute("isVoditelj")
    // public boolean isVoditelj(HttpSession session, @SessionAttribute Korisnik loggedUser) {
    //     Boolean exists = stanicaService.isVoditelj(loggedUser.getIdKorisnik());
    //     session.setAttribute("isVoditelj", exists);
    //     return exists;
    // }

    @GetMapping("/login")
    public String home(Model model)
    {
        model.addAttribute("user", new Korisnik());
                return "login";
    }

    @PostMapping("/login")
    public String loginUser(String email, String lozinka, HttpServletRequest request, RedirectAttributes redAttrs)
    {
        System.out.println(email + lozinka);
        try {
            Korisnik korisnik = korisnikService.logginKorisnik(email, lozinka);
            request.getSession().setAttribute("loggedUser", korisnik);
            request.getSession().setAttribute("dispecer", korisnik.getDispecer());
            request.getSession().setAttribute("spasilac", korisnik.getSpasilac());
            request.getSession().setAttribute("admin", korisnik.getAdministrator());
            return "redirect:/";
        }
        catch (RuntimeException e) {
            redAttrs.addFlashAttribute("error", e.getMessage());
            return "redirect:/login";
        }
    }
}
