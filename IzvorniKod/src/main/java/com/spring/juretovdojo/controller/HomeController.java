package com.spring.juretovdojo.controller;

import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.SpasilacService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class HomeController {

    @Autowired
    private StanicaService stanicaService;

    @Autowired
    private SpasilacService spasilacService;
    
    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        System.out.printf("loggedUser=%s", loggedUser);
        return loggedUser;
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }

    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        System.out.printf("loggedUser=%s", loggedUser);
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("nazivstanice")
    public String korisnikStanica(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return "";
        if (spasilacService.isSpasilac(loggedUser.getIdKorisnik())) {
            Long idStanica = loggedUser.getSpasilac().getIdStanica();
            if (idStanica == null) return "";
        }
        String naziv = spasilacService.isSpasilac(loggedUser.getIdKorisnik()) ?
        stanicaService.getStanica(loggedUser.getSpasilac().getIdStanica()).getImeStanice() : "";
        return naziv;
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @GetMapping({"/", "/home", ""})
    public String home()
    {
        return "home";
    }
}
