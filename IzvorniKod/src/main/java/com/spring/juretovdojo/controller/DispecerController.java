package com.spring.juretovdojo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Komentar;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.model.Zadatak;
import com.spring.juretovdojo.service.AkcijaService;
import com.spring.juretovdojo.service.DispecerService;
import com.spring.juretovdojo.service.KomentarService;
import com.spring.juretovdojo.service.NaciniOsposobljenostiService;
import com.spring.juretovdojo.service.SpasilacService;
import com.spring.juretovdojo.service.StanicaService;
import com.spring.juretovdojo.service.ZadatakService;
import com.spring.juretovdojo.service.ZahtjevService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class DispecerController {

    @Autowired
    private DispecerService dispecerService;

    @Autowired
    private StanicaService stanicaService;

    @Autowired
    private AkcijaService akcijaService;

    @Autowired
    private ZahtjevService zahtjevService;

    @Autowired
    private ZadatakService zadatakService;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private KomentarService komentarService;

    @Autowired
    private NaciniOsposobljenostiService naciniOsposobljenostiService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @ModelAttribute("loggedInType")
    public String loggedInType(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return "error";
        if (loggedUser.getSpasilac() != null) return "Spasilac";

        if (loggedUser.getDispecer() != null) return "Dispecer";

        if (loggedUser.getAdministrator() != null) return "Admin";
        return "error";
    }

    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }

    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }

    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("isDispecerLoggedIn")
    public boolean isDispecer(HttpSession session, @SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return false;
        boolean isDispecer = dispecerService.isDispecer(loggedUser.getIdKorisnik());
        if (isDispecer) {
            session.setAttribute("isDispecerLoggedIn", true);
            return true;
        }
        else {
            session.setAttribute("isDispecerLoggedIn", false);
            return false;
        }
    }

    @ModelAttribute("allTasks")
    public List<Zadatak> getAllZadaci(@SessionAttribute(name = "dispecer") Dispecer dispecer) {
        return zadatakService.getAll();
    }

    @ModelAttribute("allSaviours")
    public List<Spasilac> allSaviours() {
        return spasilacService.getAllSpasilac();
    }
    
    @ModelAttribute("allComments")
    public List<Komentar> allComments(@SessionAttribute(name = "dispecer", required = false) Dispecer dispecer) {
        return komentarService.getAll();
    }

    @ModelAttribute("allStations")
    public List<Map<String,String>> allStations() {
        List<Stanica> stanice = stanicaService.getAllStations();
    List<Map<String, String>> stations = new ArrayList<>();
    
    stanice.
    stream().forEach(s -> {
        Map<String, String> mapirano = new HashMap<>();
        mapirano.put("idStanice", s.getIdStanica().toString());
        mapirano.put("imeStanice", s.getImeStanice());
        mapirano.put("sirina", Float.toString(s.getLokacija().getSirina()));
        mapirano.put("duzina", Float.toString(s.getLokacija().getDuzina()));
        mapirano.put("brojSpasilaca", Integer.toString(s.getSpasilaci().size()));
        stations.add(mapirano);
    });
    return stations;
    }

    @ModelAttribute("allActions")
    public List<Map<String,String>> allActions(@SessionAttribute(required = false, name = "dispecer") Dispecer dispecer) {
        if (dispecer == null) return null;
        List<Akcija> akcije = akcijaService.getDispecerActions(dispecer);
        List<Map<String, String>> actions = new ArrayList<>();
        akcije.
        stream().forEach(s -> {
            Map<String, String> mapirano = new HashMap<>();
            mapirano.put("actionId", s.getIdAkcija().toString());
            mapirano.put("active", Boolean.toString(s.getStatus()));
            mapirano.put("sirina", Float.toString(s.getLokacija().getSirina()));
            mapirano.put("duzina", Float.toString(s.getLokacija().getDuzina()));
            actions.add(mapirano);
        }
        );
        return actions;
    }
    
    @GetMapping("/dispecer")
    public String dispecer(Model model, @SessionAttribute boolean isDispecerLoggedIn)
    {
        if (isDispecerLoggedIn)
        {
            return "dispecer";
        }
        return "redirect:/invalidCredentials";
    }

    @GetMapping("/dispecer/action/request/{actionid}")
    public String actionRequestGet(Model model, @SessionAttribute(required = false) Dispecer dispecer,
    @PathVariable(name = "actionid") Long idAction) {
        if (dispecer == null) {
            return "redirect:/invalidCredentials";
        }
        model.addAttribute("nacini", naciniOsposobljenostiService.getAll());
        model.addAttribute("actionId", idAction);
        return "request";
    }

    @PostMapping("/dispecer/request")
    public String actionRequestPost(Model model,
    @RequestParam(name = "actionId") Long idAction,
    @RequestParam(name = "transportId") List<Long> idTransports,
    @RequestParam(name = "razinahitnosti") Integer razinaHitnosti,
    @RequestParam(name = "info") String info,
    @SessionAttribute(name = "dispecer", required = false) Dispecer dispecer,
    RedirectAttributes redAttrs) {
        if (dispecer == null) return "redirect:/invalidCredentials";

        zahtjevService.createZahtjevi(dispecer, idAction, idTransports, razinaHitnosti, info);
        return "redirect:/dispecer";
    }

    @GetMapping("dispecer/action/{id}")
    public String action(Model model, @PathVariable(name = "id") Long idAkcija, @SessionAttribute(name = "dispecer") Dispecer dispecer) {
        if (dispecer == null) {
            return "redirect:/invalidCredentials";
        }
        System.out.println(model.containsAttribute("allSaviours")); 
        model.addAttribute("action", akcijaService.getAkcija(idAkcija));
        model.addAttribute("allSaviours", spasilacService.getAllSpasilacForAkcija(idAkcija));
        model.addAttribute("allComments", komentarService.getAllCommentOnAction(idAkcija));
        model.addAttribute("allTasks", zadatakService.getAllTasksOnAction(idAkcija));
        return "actiondispatcher";
    }

    @PostMapping("/assignTask")
    public String assignTask(@RequestParam(name = "actionSavior") Long idSaviour, @RequestParam(name = "location") String location,
    @RequestParam(name = "komentar") String komentar, @RequestParam(name = "actionId") Long idAction, 
    @SessionAttribute(name = "dispecer") Dispecer dispecer, RedirectAttributes redAttrs) {
        zadatakService.makeZadatak(idSaviour, dispecer.getIdDispecer(), idAction, komentar, location);
        return "redirect:/dispecer";

    }
}
