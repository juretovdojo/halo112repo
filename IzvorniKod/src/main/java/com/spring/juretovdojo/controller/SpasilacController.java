package com.spring.juretovdojo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.juretovdojo.model.Akcija;
import com.spring.juretovdojo.model.Dispecer;
import com.spring.juretovdojo.model.Komentar;
import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.NaciniOsposobljenosti;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.Stanica;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.model.Zadatak;
import com.spring.juretovdojo.model.Zahtjev;
import com.spring.juretovdojo.service.AkcijaService;
import com.spring.juretovdojo.service.KomentarService;
import com.spring.juretovdojo.service.NaciniOsposobljenostiService;
import com.spring.juretovdojo.service.SpasilacService;
import com.spring.juretovdojo.service.StanicaService;
import com.spring.juretovdojo.service.ZadatakService;
import com.spring.juretovdojo.service.ZahtjevService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

@Controller
public class SpasilacController {

    @Autowired
    private StanicaService stanicaService;

    @Autowired
    private SpasilacService spasilacService;

    @Autowired
    private NaciniOsposobljenostiService naciniOpsService;

    @Autowired
    private AkcijaService akcijaService;

    @Autowired
    private ZahtjevService zahtjevService;

    @Autowired
    private ZadatakService zadatakService;

    @Autowired
    private KomentarService komentarService;

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    
    @ModelAttribute("spasilac")
    public Spasilac loggedInSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac;
    }
    
    @ModelAttribute("dispecer")
    public Dispecer loggedInSpasilac(@SessionAttribute(required = false) Dispecer dispecer) {
        return dispecer;
    }
    
    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }
    
    @ModelAttribute("loggedInType")
    public String loggedInType(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser.getSpasilac() != null) return "Spasilac";
        
        if (loggedUser.getDispecer() != null) return "Dispecer";
        
        if (loggedUser.getAdministrator() != null) return "Admin";
        return "greska";
    }
    
    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("Status")
    public Boolean statusSpasilac(@SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean status = spasilacService.getSpasilac(spasilac.getIdSpasilac()).getDostupnost();
        return status;
    }
    
    @ModelAttribute("isSpasilac")
    public boolean isSpasilac(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        return spasilac != null;
    }
    
    @ModelAttribute("nazivstanice")
    public String korisnikStanica(@SessionAttribute(required = false) Spasilac spasilac) {
        Long idStanica = spasilac.getIdStanica();
        if (idStanica == null) return "";
        String naziv = spasilac == null ? "" : stanicaService.getStanica(idStanica).getImeStanice();
        return naziv;
    }

    @ModelAttribute("currentSaviour")
    public Spasilac currentSaviour(@SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return null;
        return spasilacService.getSpasilac(spasilac.getIdSpasilac());
    }
    
    
    @ModelAttribute("allTasks")
    public List<Zadatak> getAllZadaci(@SessionAttribute(name = "spasilac", required = false) Spasilac spasilac) {
        Spasilac sp = spasilacService.getSpasilac(spasilac);
        return zadatakService.getAllTasksForSpasilac(sp);
    }

    @ModelAttribute("allSaviours")
    public List<Spasilac> allSavioursOnSameAction(@SessionAttribute(name = "spasilac", required = false) Spasilac spasilac) {
        Spasilac sp = spasilacService.getSpasilac(spasilac);
        System.out.println();
        List<Spasilac> listaSpasilaca = new ArrayList<>();
        listaSpasilaca.addAll(spasilacService.getAllOnSpasilacAction(sp));
        return listaSpasilaca;
    }
    
    @ModelAttribute("allComments")
    public List<Komentar> allComments(@SessionAttribute(name = "dispecer", required = false) Spasilac spasilac) {
        Spasilac sp = spasilacService.getSpasilac(spasilac);
        return komentarService.findKomentariOnSameAction(sp);
    }
    
    @ModelAttribute("allActions")
    public List<Map<String,String>> allActions(@SessionAttribute(required = false, name = "spasilac") Spasilac spasilac) {
        if (spasilac == null) return null;
        Akcija akcija = akcijaService.getSpasilacAction(spasilac);
        
        List<Akcija> akcije = new ArrayList<>();
        List<Map<String, String>> actions = new ArrayList<>();
        
        if (akcija == null) return actions;
        akcije.add(akcija);
        
        akcije.
        stream().forEach(s -> {
            Map<String, String> mapirano = new HashMap<>();
            mapirano.put("actionId", s.getIdAkcija().toString());
            mapirano.put("active", Boolean.toString(s.getStatus()));
            mapirano.put("sirina", Float.toString(s.getLokacija().getSirina()));
            mapirano.put("duzina", Float.toString(s.getLokacija().getDuzina()));
            actions.add(mapirano);
        }
        );
        return actions; 
    }
    
    @ModelAttribute("allStations")
    public List<Map<String,String>> allStations() {
        List<Stanica> stanice = stanicaService.getAllStations();
        List<Map<String, String>> stations = new ArrayList<>();
        
        stanice.
        stream().forEach(s -> {
            Map<String, String> mapirano = new HashMap<>();
            mapirano.put("idStanice", s.getIdStanica().toString());
            mapirano.put("imeStanice", s.getImeStanice());
            mapirano.put("sirina", Float.toString(s.getLokacija().getSirina()));
            mapirano.put("duzina", Float.toString(s.getLokacija().getDuzina()));
            mapirano.put("brojSpasilaca", Float.toString(s.getSpasilaci().size()));
            stations.add(mapirano);
        }
        );
        return stations;
    }

    @GetMapping("/spasilac")
    public String home(Model model, @SessionAttribute(name = "spasilac", required = false) Spasilac spasilac)
    {
        if (spasilac == null)
        {
            return "redirect:/invalidCredentials";
        }
        model.addAttribute("station", new Stanica());
        return "spasilac";
    }

    @PostMapping("/updateSaviorStatus")
    public String updateSaviourStatus(@RequestParam(name = "spasilacStatus") Boolean status, 
    @SessionAttribute(required = false) Spasilac spasilac) {
        Spasilac spasilac2 = spasilacService.getSpasilac(spasilac.getIdSpasilac());
        spasilac2.setDostupnost(status);
        spasilacService.saveSpasilac(spasilac2);
        return "redirect:/spasilac";
    }

    @GetMapping("/spasilac/actionspasilac")
    public String actionsaviour(Model model, @SessionAttribute(name = "spasilac") Spasilac spasilac) {
        if (spasilac == null) {
            return "redirect:/InvalidCredentials";
        }
        Long idAkcije = spasilacService.getSpasilac(spasilac.getIdSpasilac()).getIdAkcije();
        if (idAkcije != null) {
            Akcija akcija = akcijaService.getAkcija(spasilacService.getSpasilac(spasilac.getIdSpasilac()).getIdAkcije());
            model.addAttribute("action", akcija);
        } else {
            model.addAttribute("action", new Akcija());
        };
        return "actionsaviour";
    }

    @GetMapping("/{nazivstanice}/edittransport/{id}")
    public String edittransport(Model model, @PathVariable(value = "id") Long saviourId, @SessionAttribute(required = false) Spasilac spasilac) {
        if (!stanicaService.isVoditelj(spasilac)) {
            return "redirect:/invalidCredentials";
        }
        Spasilac editedUser = spasilacService.getSpasilac(saviourId);
        model.addAttribute("user", editedUser);
        List<NaciniOsposobljenosti> sveOsposobljenosti = naciniOpsService.getAll();
        model.addAttribute("nacini", sveOsposobljenosti);
        return "edittransport";
    }

    @PostMapping("/spasilac/editstationsaviour")
    public String setTransport(Model model, @RequestParam(name = "idUser") Long idSpasilac, @RequestParam(name = "transportId") List<Long> idsTransports, RedirectAttributes redAttrs) {
        try {
            spasilacService.setTransportsForSpasilac(idSpasilac, idsTransports);
            redAttrs.addFlashAttribute("success", "Uspješno dodani transporti");
            return "redirect:/home";
        }
        catch (RuntimeException err) {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/home";
        }
    }

    @GetMapping("/saviourrequest")
    public @ResponseBody List<Zahtjev> getZahtjeviForSpasilac(@SessionAttribute(name = "spasilac") Spasilac spasilac) {
        if (spasilac == null) return null;

        List<Zahtjev> zahtjevi = zahtjevService.getZahtjeviForSpasilac(spasilac);
        return zahtjevi;
    }

    @PostMapping("saviourresponse")
    public @ResponseBody List<Map<String,String>> responseForRequest(@RequestBody String req, @SessionAttribute(name = "spasilac") Spasilac spasilac) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode jsonNode = objectMapper.readTree(req);
            Boolean accept = jsonNode.get("data").asBoolean();
            Long idZahtjev = jsonNode.get("idZahtjev").asLong();
            if (accept) {
                zahtjevService.prihvatiZahtjev(idZahtjev, spasilac);
                return allActions(spasilac);
            }
            zahtjevService.odbiZahtjev(idZahtjev);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/marklocation")
    public @ResponseBody Spasilac markLocation(@RequestBody String req,
    @SessionAttribute(name = "spasilac") Spasilac spasilac) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode jsonNode = objectMapper.readTree(req);
            Spasilac sp = spasilacService.saveSpasilacLocation(spasilac, jsonNode.get("sirina").asText(), jsonNode.get("duzina").asText());
            return sp;
        } catch (JsonProcessingException e) {
            System.out.println("\nGRESKA LOKACIJA SPASILAC!!!");
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping("/removestationsaviour")
    public String removeSaviourFromStation(@RequestAttribute(name = "id") Long id, RedirectAttributes redAttrs) {
        try {
            spasilacService.removeSaviourFromStation(id);
            redAttrs.addFlashAttribute("succes", "Uspješno izbačen");
            return "redirect:/dispecer";
        } catch (RuntimeException err) {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/dispecer";
        }
    }

    @PostMapping("/tasksaviours")
    public @ResponseBody List<Spasilac> getAkcijaSpasioci(@RequestBody Long idAction) {
        System.out.println(idAction);
        return spasilacService.getAllSpasilacForAkcija(idAction);
    }
}
