package com.spring.juretovdojo.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.spring.juretovdojo.model.Korisnik;
import com.spring.juretovdojo.model.Spasilac;
import com.spring.juretovdojo.model.UserType;
import com.spring.juretovdojo.service.KorisnikService;
import com.spring.juretovdojo.service.StanicaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SignupController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private StanicaService stanicaService;

    @InitBinder
    protected void InitBinder(HttpServletRequest request, ServletRequestDataBinder binder)
    {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }
    
    @ModelAttribute("role")
    public String loggedInRole(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return UserType.toCroWord(loggedUser.getVrstaSpecijalizacije());
    }

    @ModelAttribute("isVoditelj")
    public boolean isVoditelj(HttpSession session, @SessionAttribute(required = false) Spasilac spasilac) {
        if (spasilac == null) return false;
        Boolean exists = stanicaService.isVoditelj(spasilac);
        session.setAttribute("isVoditelj", exists);
        return exists;
    }

    @ModelAttribute("loggedUser")
    public Korisnik loggedUser(@SessionAttribute(required = false) Korisnik loggedUser) {
        if (loggedUser == null) return null;
        return loggedUser;
    }

    @GetMapping("/register")
    public String home(Model model) {
        model.addAttribute("user", new Korisnik());
        return "register";
    }

    @PostMapping("/registerUser")
    public String signUpUser(@ModelAttribute Korisnik korisnik, RedirectAttributes redAttrs, @RequestParam(name = "fotografija") MultipartFile photo) throws IOException
    {
        try
        {
            korisnikService.saveKorisnik(korisnik, photo);
        }
        catch (RuntimeException err)
        {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/register";
        }
        catch (IOException err)
        {
            redAttrs.addFlashAttribute("error", err.getMessage());
            return "redirect:/register";
        }
        redAttrs.addFlashAttribute("success", "Vaš zahtjev za registracijom je uspješno poslan!");
        return "redirect:/register";
    }
}
