package com.spring.juretovdojo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {
    
    @GetMapping("/invalidCredentials")
    public String invalidCredentials() {
        return "invalidCredentials";
    }
}
