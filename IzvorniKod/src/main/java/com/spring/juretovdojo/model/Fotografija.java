package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Fotografija {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFotografija;
    private Long idAkcija;
    private byte[] fotografija;

    public Fotografija() {
    }

    public Long getIdFotografija() {
        return this.idFotografija;
    }

    public void setIdFotografija(Long idFotografija) {
        this.idFotografija = idFotografija;
    }

    public Long getIdAkcija() {
        return this.idAkcija;
    }

    public void setIdAkcija(Long idAkcija) {
        this.idAkcija = idAkcija;
    }

    public byte[] getFotografija() {
        return this.fotografija;
    }

    public void setFotografija(byte[] fotografija) {
        this.fotografija = fotografija;
    }

    public String getPhotoInString()
    {
        return new String(this.getFotografija());
    }

    @Override
    public String toString() {
        return "{" +
            " idFotografija='" + getIdFotografija() + "'" +
            ", idAkcija='" + getIdAkcija() + "'" +
            ", fotografija='" + getFotografija() + "'" +
            "}";
    }
    
}
