package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NaciniOsposobljenosti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOsposobljenosti;
    
    private String nazivOsposobljenosti;

    public NaciniOsposobljenosti() {
    }

    public NaciniOsposobljenosti(String nazivOsposobljenosti) {
        this.nazivOsposobljenosti = nazivOsposobljenosti;
    }

    public Long getIdOsposobljenosti() {
        return this.idOsposobljenosti;
    }

    public void setIdOsposobljenosti(Long idOsposobljenosti) {
        this.idOsposobljenosti = idOsposobljenosti;
    }

    public String getNazivOsposobljenosti() {
        return this.nazivOsposobljenosti;
    }

    public void setNazivOsposobljenosti(String nazivOsposobljenosti) {
        this.nazivOsposobljenosti = nazivOsposobljenosti;
    }

    @Override
    public String toString() {
        return "{" +
            " idOsposobljenosti='" + getIdOsposobljenosti() + "'" +
            ", nazivOsposobljenosti='" + getNazivOsposobljenosti() + "'" +
            "}";
    }
}
