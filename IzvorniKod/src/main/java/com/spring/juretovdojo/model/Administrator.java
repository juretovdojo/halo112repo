package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
public class Administrator
{
    @Id
    private Long idAdministrator;
    
    @OneToOne
    @PrimaryKeyJoinColumn
    private Korisnik korisnik;
    public Korisnik getKorisnik()
    {
        return this.korisnik;
    }


    public Administrator() {
    }

    public Administrator(Long IdAdministrator) {
        this.idAdministrator = IdAdministrator;
    }

    public Long getIdAdministrator() {
        return idAdministrator;
    }

    public void setIdAdministrator(Long IdAdministrator) {
        this.idAdministrator = IdAdministrator;
    }

    @Override
    public String toString() {
        return "{" +
            " idAdministrator='" + getIdAdministrator() + "'" +
            ", korisnik='" + getKorisnik() + "'" +
            "}";
    }

}
