package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Zahtjev {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idZahtjev; 
    private Long idDispecer;
    private Long idAkcija;
    private Long idSpasilac;
    private int razinaHitnosti;
    private String informacija;

    public Zahtjev() {
    }

    public Long getIdZahtjev() {
        return this.idZahtjev;
    }

    public void setIdZahtjev(Long idZahtjev) {
        this.idZahtjev = idZahtjev;
    }

    public Long getIdDispecer() {
        return this.idDispecer;
    }

    public void setIdDispecer(Long idDispecer) {
        this.idDispecer = idDispecer;
    }

    public Long getIdAkcija() {
        return this.idAkcija;
    }

    public void setIdAkcija(Long idAkcija) {
        this.idAkcija = idAkcija;
    }

    public Long getIdSpasilac() {
        return this.idSpasilac;
    }

    public void setIdSpasilac(Long idSpasilac) {
        this.idSpasilac = idSpasilac;
    }

    public int getRazinaHitnosti() {
        return this.razinaHitnosti;
    }

    public void setRazinaHitnosti(int razinaHitnosti) {
        this.razinaHitnosti = razinaHitnosti;
    }

    public String getInformacija() {
        return this.informacija;
    }

    public void setInformacija(String informacija) {
        this.informacija = informacija;
    }

    @Override
    public String toString() {
        return "{" +
            " idZahtjev='" + getIdZahtjev() + "'" +
            ", idDispecer='" + getIdDispecer() + "'" +
            ", idAkcija='" + getIdAkcija() + "'" +
            ", idSpasilac='" + getIdSpasilac() + "'" +
            ", razinaHitnosti='" + getRazinaHitnosti() + "'" +
            ", informacija='" + getInformacija() + "'" +
            "}";
    }
   
}
