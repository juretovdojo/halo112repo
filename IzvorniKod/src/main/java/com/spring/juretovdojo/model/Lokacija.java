package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Lokacija {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idLokacija;
    private float sirina;
    private float duzina;
    private String lokacija;

    public Lokacija() {
    }

    public Lokacija(float sirina, float duzina, String lokacija) {
        this.sirina = sirina;
        this.duzina = duzina;
        this.lokacija = lokacija;
    }

    public Long getIdLokacija() {
        return this.idLokacija;
    }

    public void setIdLokacija(Long idLokacija) {
        this.idLokacija = idLokacija;
    }

    public float getSirina() {
        return this.sirina;
    }

    public void setSirina(float sirina) {
        this.sirina = sirina;
    }

    public float getDuzina() {
        return this.duzina;
    }

    public void setDuzina(float duzina) {
        this.duzina = duzina;
    }

    public String getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    @Override
    public String toString() {
        return "{" +
            " idLokacija='" + getIdLokacija() + "'" +
            ", sirina='" + getSirina() + "'" +
            ", duzina='" + getDuzina() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            "}";
    }

    


}
