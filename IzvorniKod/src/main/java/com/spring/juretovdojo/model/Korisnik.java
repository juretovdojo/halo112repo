package com.spring.juretovdojo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
public class Korisnik
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKorisnik;
    private String ime;
    private String prezime;
    @Column(unique = true)
    private String brojMobitela;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String korisnickoIme;
    @JsonIgnore
    private String lozinka;
    private boolean registriran;
    private UserType vrstaSpecijalizacije;
    @JsonIgnore
    private byte[] fotografija;
    
    @OneToOne(mappedBy = "korisnik", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Spasilac spasilac;
    @JsonIgnore
    public Spasilac getSpasilac() {
        return this.spasilac;
    }

    public void setSpasilac(Spasilac spasilac) {
        this.spasilac = spasilac;
    }

    @OneToOne(mappedBy = "korisnik", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Administrator administrator;
    @JsonIgnore
    public Administrator getAdministrator() {
        return this.administrator;
    }
    
    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    @OneToOne(mappedBy = "korisnik", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Dispecer dispecer;
    @JsonIgnore
    public Dispecer getDispecer() {
        return this.dispecer;
    }

    public void setDispecer(Dispecer dispecer) {
        this.dispecer = dispecer;
    }

    public Korisnik() {
    }

    public Korisnik(Long idKorisnik, String ime, String prezime, byte[] fotografija, String brojMobitela, String email, String korisnickoIme, String lozinka, boolean registriran, UserType vrstaSpecijalizacije) {
        this.idKorisnik = idKorisnik;
        this.ime = ime;
        this.prezime = prezime;
        this.fotografija = fotografija;
        this.brojMobitela = brojMobitela;
        this.email = email;
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.registriran = registriran;
        this.vrstaSpecijalizacije = vrstaSpecijalizacije;
    }

    public Long getIdKorisnik() {
        return this.idKorisnik;
    }

    public void setIdKorisnik(Long IdKorisnik) {
        this.idKorisnik = IdKorisnik;
    }

    public String getIme() {
        return this.ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return this.prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
    
    @Lob
    public byte[] getFotografija() {
        return this.fotografija;
    }

    public void setFotografija(byte[] fotografija) {
        this.fotografija = fotografija;
    }

    public String getBrojMobitela() {
        return this.brojMobitela;
    }

    public void setBrojMobitela(String brojMobitela) {
        this.brojMobitela = brojMobitela;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKorisnickoIme() {
        return this.korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return this.lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public boolean isRegistriran() {
        return this.registriran;
    }

    public boolean getRegistriran() {
        return this.registriran;
    }

    public void setRegistriran(boolean registriran) {
        this.registriran = registriran;
    }
    @JsonIgnore
    public UserType getVrstaSpecijalizacije() {
        return this.vrstaSpecijalizacije;
    }
    public String getVrstaSpecijalizacijeString() {
        return this.vrstaSpecijalizacije.toString();
    }

    public void setVrstaSpecijalizacije(UserType vrstaSpecijalizacije) {
        this.vrstaSpecijalizacije = vrstaSpecijalizacije;
    }

    @Override
    public String toString() {
        return "{" +
            " idKorisnik='" + getIdKorisnik() + "'" +
            ", ime='" + getIme() + "'" +
            ", prezime='" + getPrezime() + "'" +
            ", fotografija='" + getFotografija() + "'" +
            ", brojMobitela='" + getBrojMobitela() + "'" +
            ", email='" + getEmail() + "'" +
            ", korisnickoIme='" + getKorisnickoIme() + "'" +
            ", lozinka='" + getLozinka() + "'" +
            ", registriran='" + isRegistriran() + "'" +
            ", vrstaSpecijalizacije='" + getVrstaSpecijalizacije() + "'" +
            "}\n";
    }
   
    public boolean signinWrong()
    {
        if (this.getIme() == null || this.getPrezime() == null || this.getFotografija() == null
        || this.getBrojMobitela() == null ||this.getEmail() == null || this.getKorisnickoIme() == null
        || this.getVrstaSpecijalizacije() == null) return true;
        return false;
    }

    @JsonIgnore
    public String getPhotoInString()
    {
        return new String(this.getFotografija());
    }
    
    public String validateUser()
    {
    	if (this.getIme().length() < 2) {
    		return "Ime je prekratko!";
    	} else if (this.getPrezime().length() < 2 || this.getPrezime().length() > 15) {
    		return "Broj znakova u prezimenu mora biti najmanje 2, a najviše 15.";
    	} else if (this.getKorisnickoIme().length() < 5 || this.getKorisnickoIme().length() > 15) {
    		return "Broj znakova u korisničkom imenu mora biti najmanje 5, a najviše 15.";
    	} 
    	Pattern p = Pattern.compile("^([+]?\\d{1,2}[-\\s]?|)\\d{3}[-\\s]?\\d{3}[-\\s]?\\d{4}$");
    	Matcher m = p.matcher(this.getBrojMobitela());
    	if (!(m.find()) || this.getBrojMobitela().length() < 9 || this.getBrojMobitela().length() > 15)
    		return "Broj mobitela nije važeći.";
    	p = Pattern.compile("^([a-z0-9]+(?:[._-][a-z0-9]+)*)@([a-z0-9]+(?:[.-][a-z0-9]+)*\\.[a-z]{2,})$");
    	m = p.matcher(this.getEmail());
    	if (!(m.find()))
    		return "Email nije važeći.";
    	return "OK";
    }
}
