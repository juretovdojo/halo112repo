package com.spring.juretovdojo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Stanica {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idStanica;
    private String imeStanice;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idVoditelj", referencedColumnName = "idSpasilac")
    private Spasilac voditelj;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "idLokacija", referencedColumnName = "idLokacija")
    private Lokacija lokacija;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "idstanica", referencedColumnName = "idstanica")
    @JsonIgnore
    private Set<Spasilac> spasilaci;

    public Set<Spasilac> getSpasilaci() {
        return this.spasilaci;
    }

    public Stanica() {
    }

    public Stanica(String imeStanice, Lokacija lokacija, Spasilac voditelj) {
        this.imeStanice = imeStanice;
        this.lokacija = lokacija;
        this.voditelj = voditelj;
    }

    public Long getIdStanica() {
        return this.idStanica;
    }

    public void setIdStanica(Long idStanica) {
        this.idStanica = idStanica;
    }

    public String getImeStanice() {
        return this.imeStanice;
    }

    public void setImeStanice(String imeStanice) {
        this.imeStanice = imeStanice;
    }

    public Spasilac getVoditelj() {
        return this.voditelj;
    }

    public void setVoditelj(Spasilac voditelj) {
        this.voditelj = voditelj;
    }

    public Lokacija getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }
    

    @Override
    public String toString() {
        return "{" +
            " idStanica='" + getIdStanica() + "'" +
            ", imeStanice='" + getImeStanice() + "'" +
            ", voditelj='" + getVoditelj() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            "}";
    }


}
