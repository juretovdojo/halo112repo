package com.spring.juretovdojo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Zadatak {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idzadatak; 
    private Long idSpasilac;
    private Long idDispecer;
    private String opis;
    private Long idAkcija;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "lokacijapocetak", referencedColumnName = "idlokacija")
    private Lokacija lokacija;

    public Zadatak() {
    }

    public Long getIdzadatak() {
        return this.idzadatak;
    }

    public void setIdzadatak(Long idzadatak) {
        this.idzadatak = idzadatak;
    }

    public Long getIdSpasilac() {
        return this.idSpasilac;
    }

    public void setIdSpasilac(Long idSpasilac) {
        this.idSpasilac = idSpasilac;
    }

    public Long getIdDispecer() {
        return this.idDispecer;
    }

    public void setIdDispecer(Long idDispecer) {
        this.idDispecer = idDispecer;
    }

    public String getOpis() {
        return this.opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Long getIdAkcija() {
        return this.idAkcija;
    }

    public void setIdAkcija(Long idAkcija) {
        this.idAkcija = idAkcija;
    }

    public Lokacija getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }

    @Override
    public String toString() {
        return "{" +
            " idzadatak='" + getIdzadatak() + "'" +
            ", idSpasilac='" + getIdSpasilac() + "'" +
            ", idDispecer='" + getIdDispecer() + "'" +
            ", opis='" + getOpis() + "'" +
            ", idAkcija='" + getIdAkcija() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            "}";
    }

}
