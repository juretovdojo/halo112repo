package com.spring.juretovdojo.model;

public enum UserType
{
    ADMIN,
    DISPATCHER,
    LEAD_SAVIOUR,
    POLICEMAN,
    FIREMAN,
    DOCTOR;

    public static UserType stringToEnum(String role)
    {
        switch(role.toUpperCase())
        {
            case "ADMIN": return UserType.ADMIN;
            case "DISPATCHER": return UserType.DISPATCHER;
            case "LEAD_SAVIOUR": return UserType.LEAD_SAVIOUR;
            case "POLICEMAN": return UserType.POLICEMAN;
            case "FIREMAN": return UserType.FIREMAN;
            case "DOCTOR": return UserType.DOCTOR;
            default: return null;
        }
    }

    public static String toCroWord(UserType role)
    {
        switch(role)
        {
            case ADMIN: return "Admin";
            case DISPATCHER: return "Dispečer";
            case LEAD_SAVIOUR: return "Glavni spasilac";
            case POLICEMAN: return "Policajac";
            case FIREMAN: return "Vatrogasac";
            case DOCTOR: return "Doktor";
            default: return null;
        }
    }
}

