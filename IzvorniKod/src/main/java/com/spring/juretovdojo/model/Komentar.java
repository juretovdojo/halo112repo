package com.spring.juretovdojo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Komentar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKomentar; 
    private Long idKorisnik;
    private Long idAkcija;
    private String tekst;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idlokacija", referencedColumnName = "idlokacija")
    private Lokacija lokacija;

    public Long getIdKomentar() {
        return this.idKomentar;
    }

    public void setIdKomentar(Long idKomentara) {
        this.idKomentar = idKomentara;
    }

    public Long getIdKorisnik() {
        return this.idKorisnik;
    }

    public void setIdKorisnik(Long idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public Long getIdAkcija() {
        return this.idAkcija;
    }

    public void setIdAkcija(Long idAkcije) {
        this.idAkcija = idAkcije;
    }

    public String getTekst() {
        return this.tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public Lokacija getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }

    @Override
    public String toString() {
        return "{" +
            " idKomentara='" + getIdKomentar() + "'" +
            ", idKorisnik='" + getIdKorisnik() + "'" +
            ", idAkcije='" + getIdAkcija() + "'" +
            ", tekst='" + getTekst() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            "}";
    }
}
