package com.spring.juretovdojo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Dispecer {

    
    @Id
    private Long idDispecer;
    
    @OneToOne
    @PrimaryKeyJoinColumn
    private Korisnik korisnik;
    
    public Korisnik getKorisnik() {
        return this.korisnik;
    }

    public Dispecer() {
    }

    public Dispecer(long id) {
        this.idDispecer = id;
    }
    public Dispecer(Korisnik korisnik) {
        this.idDispecer = korisnik.getIdKorisnik();
    }

    public Long getIdDispecer() {
        return this.idDispecer;
    }

    public void setIdDispecer(Long idDispecer) {
        this.idDispecer = idDispecer;
    }

    @Override
    public String toString() {
        return "{" +
            " idDispecer='" + getIdDispecer() + "'" +
            ", korisnik='" + getKorisnik() + "'" +
            "}";
    }
}
