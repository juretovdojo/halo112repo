package com.spring.juretovdojo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Spasilac {

    @Id
    private Long idSpasilac; 
    private Long idStanica;
    private Boolean dostupnost = true;
    private Long idAkcija;

    @OneToOne(fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Korisnik korisnik;
    public Korisnik getKorisnik() {
        return this.korisnik;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "jeosposobljen",
        joinColumns = @JoinColumn(name = "idspasilac", referencedColumnName = "idspasilac"),
        inverseJoinColumns = @JoinColumn(name = "idosposobljenosti", referencedColumnName = "idosposobljenosti"))
    List<NaciniOsposobljenosti> osposobljenosti;

    public void setOsposobljenosti(List<NaciniOsposobljenosti> osposobljenosti) {
        this.osposobljenosti = osposobljenosti;
    }

    public List<NaciniOsposobljenosti> getOsposobljenosti() {
        return this.osposobljenosti;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idlokacija", referencedColumnName = "idlokacija")
    private Lokacija lokacija;

    public Spasilac() {
    }

    public Spasilac(long idSpasilac) {
        this.idSpasilac = idSpasilac;
    }

    public Spasilac(Korisnik korisnik) {
        this(korisnik.getIdKorisnik());
    }

    public Spasilac(Long idSpasilac, Long idAkcija, Long idStanica, boolean dostupnost) {
        this.idSpasilac = idSpasilac;
        this.idAkcija = idAkcija;
        this.idStanica = idStanica;
        this.dostupnost = dostupnost;
    }

    public Long getIdSpasilac() {
        return this.idSpasilac;
    }

    public void setIdSpasilac(Long idSpasilac) {
        this.idSpasilac = idSpasilac;
    }

    public Long getIdStanica() {
        return this.idStanica;
    }

    public void setIdStanica(Long idStanica) {
        this.idStanica = idStanica;
    }

    public boolean isDostupnost() {
        return this.dostupnost;
    }

    public boolean getDostupnost() {
        return this.dostupnost;
    }

    public void setDostupnost(boolean dostupnost) {
        this.dostupnost = dostupnost;
    }

    public Long getIdAkcije() {
        return this.idAkcija;
    }

    public void setIdAkcije(Long idAkcija) {
        this.idAkcija = idAkcija;
    }
    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Lokacija getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }

    @Override
    public String toString() {
        return "{" +
            " idSpasilac='" + getIdSpasilac() + "'" +
            ", idStanica='" + getIdStanica() + "'" +
            ", dostupnost='" + isDostupnost() + "'" +
            ", idAkcija='" + getIdAkcije() + "'" +
            ", korisnik='" + getKorisnik() + "'" +
            ", osposobljenosti='" + getOsposobljenosti() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            "}";
    }
    
}
