package com.spring.juretovdojo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "akcijaspasavanja")
public class Akcija {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAkcija;
    private boolean status = true;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idLokacija", referencedColumnName = "idLokacija")
    private Lokacija lokacija;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idpokretac", referencedColumnName = "iddispecer")
    @JsonIgnore
    private Dispecer pokretac;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "idakcija", referencedColumnName = "idakcija")
    @JsonIgnore
    private Set<Fotografija> fotografije;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "idakcija", referencedColumnName = "idakcija")
    @JsonIgnore
    private Set<Spasilac> spasilaci;

    public Set<Spasilac> getSpasilaci() {
        return spasilaci;
    }

    public Akcija() {
    }

    public Long getIdAkcija() {
        return this.idAkcija;
    }

    public void setIdAkcija(Long idAkcija) {
        this.idAkcija = idAkcija;
    }

    public boolean isStatus() {
        return this.status;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Lokacija getLokacija() {
        return this.lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }

    public Dispecer getPokretac() {
        return this.pokretac;
    }

    public void setPokretac(Dispecer pokretac) {
        this.pokretac = pokretac;
    }

    public Set<Fotografija> getFotografije() {
        return this.fotografije;
    }

    public void setFotografije(Set<Fotografija> fotografije) {
        this.fotografije = fotografije;
    }

    @Override
    public String toString() {
        return "{" +
            " idAkcija='" + getIdAkcija() + "'" +
            ", status='" + isStatus() + "'" +
            ", lokacija='" + getLokacija() + "'" +
            ", pokretac='" + getPokretac() + "'" +
            ", fotografije='" + getFotografije() + "'" +
            ", spasilaci='" + getSpasilaci() + "'" +
            "}";
    }

}
