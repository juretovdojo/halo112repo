package com.spring.juretovdojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JuretovdojoApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(JuretovdojoApplication.class, args);
	}
}
